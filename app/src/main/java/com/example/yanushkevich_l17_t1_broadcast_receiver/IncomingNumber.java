package com.example.yanushkevich_l17_t1_broadcast_receiver;

public class IncomingNumber {
    private String time;
    private String number;

    public IncomingNumber(String time, String number){
        this.time = time;
        this.number = number;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}

