package com.example.yanushkevich_l17_t1_broadcast_receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.telephony.TelephonyManager;
import android.widget.Toast;

import java.util.Calendar;

public class NumberReceiever extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        String state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
        String intentAction = intent.getAction();
       if (intentAction != null) {
            String message = "";
            Calendar c = Calendar.getInstance();
            int minut = c.get(Calendar.MINUTE);
            int hour = c.get(Calendar.HOUR_OF_DAY);
            String data = hour + ":" + minut;
            DbHelper dbHelper = new DbHelper(context);
            SQLiteDatabase database = dbHelper.getWritableDatabase();
            switch (intentAction) {
                case Intent.ACTION_POWER_CONNECTED:
                    message = "Зарядное устройство подключено";
                    dbHelper.saveInfo(message, data, database);
                    break;
                case Intent.ACTION_POWER_DISCONNECTED:
                    message = "Зарядное устройство отключено";
                    dbHelper.saveInfo(message, data, database);
                    break;
            }
            dbHelper.close();
        }
       if (state != null && state.equals(TelephonyManager.EXTRA_STATE_RINGING)){
            Calendar c = Calendar.getInstance();
            int minut = c.get(Calendar.MINUTE);
            int hour = c.get(Calendar.HOUR_OF_DAY);
            String data = hour + ":" + minut;
            String number = "Вам звонил номер " + intent.getExtras().getString(TelephonyManager.EXTRA_INCOMING_NUMBER);
            DbHelper dbHelper = new DbHelper(context);
            SQLiteDatabase database = dbHelper.getWritableDatabase();
            dbHelper.saveInfo(number,data, database);
            dbHelper.close();
        }

        Intent intent1 = new Intent(DbContract.UPDATE_UI_FILTER);
        context.sendBroadcast(intent1);
    }
}
