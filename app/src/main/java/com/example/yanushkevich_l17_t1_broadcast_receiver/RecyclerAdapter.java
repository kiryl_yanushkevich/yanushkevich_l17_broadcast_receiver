package com.example.yanushkevich_l17_t1_broadcast_receiver;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.MyViewHolder> {

    private List<IncomingNumber> numberList;

    public RecyclerAdapter(List<IncomingNumber> numberList){
        this.numberList = numberList;
    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.information_layout, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        holder.time.setText(numberList.get(position).getTime());
        holder.number.setText(numberList.get(position).getNumber());
    }

    @Override
    public int getItemCount() {
        return numberList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder{
        TextView time, number;
        public MyViewHolder(@NonNull View view) {
            super(view);
            time = view.findViewById(R.id.data_time);
            number = view.findViewById(R.id.message);
        }
    }
}
