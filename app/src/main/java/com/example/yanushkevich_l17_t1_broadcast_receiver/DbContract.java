package com.example.yanushkevich_l17_t1_broadcast_receiver;

public class DbContract {
    public static final String TABLE_NAME = "incomingInfo";
    public static final String INCOMING_NUMBER = "incomingNumber";
    public static final String INCOMING_TIME = "incomingTime";
    public static final String UPDATE_UI_FILTER = "com.example.yanushkevich_l17_t1_broadcast_receiver.UPDATE_UI";

}
